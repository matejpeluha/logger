<?php


class UserEntity
{
    private $id;
    private $first_name;
    private $surname;
    private $mail;
    private $type;
    private $user_password;
    private $google_id;
    private $secret;

    public function getId(){
        return $this->id;
    }

    public function getPassword(){
        return $this->user_password;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

}