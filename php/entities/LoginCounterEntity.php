<?php


class LoginCounterEntity
{
    private $google_counter;
    private $ldap_counter;
    private $registered_counter;


    /**
     * @return int
     */
    public function getGoogleCounter()
    {
        return $this->google_counter;
    }

    /**
     * @return int
     */
    public function getLdapCounter()
    {
        return $this->ldap_counter;
    }

    /**
     * @return int
     */
    public function getRegisteredCounter()
    {
        return $this->registered_counter;
    }

    /**
     * @param int $google_counter
     */
    public function setGoogleCounter($google_counter)
    {
        $this->google_counter = $google_counter;
    }

    /**
     * @param int $ldap_counter
     */
    public function setLdapCounter($ldap_counter)
    {
        $this->ldap_counter = $ldap_counter;
    }


    /**
     * @param int $registered_counter
     */
    public function setRegisteredCounter($registered_counter)
    {
        $this->registered_counter = $registered_counter;
    }
}