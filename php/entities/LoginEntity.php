<?php


class LoginEntity
{
    private $timestamp;

    public function getTimestamp(){
        return $this->timestamp;
    }

    public function getDate(){
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Bratislava'));
        $date->setTimestamp($this->timestamp);
        return $date->format("d.m.Y H:i:s");
    }
}