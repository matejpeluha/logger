<?php
header('Content-type: application/json');

session_start();

$json = file_get_contents('php://input');

if(isset($_SESSION['logged_id']) && $_SESSION['logged_id'] !== null)
    echo json_encode(['result' => 'success']);
else
    echo json_encode(['result' => 'fail']);
