<?php
require_once("../../config.php");
header('Content-type: application/json');

$json = file_get_contents('php://input');

if($json === null) {
    echo json_encode(['result' => 'fail']);
    return;
}

$data = json_decode($json);

require_once("../../PHPGangsta/GoogleAuthenticator.php");

$ga = new PHPGangsta_GoogleAuthenticator();
$checkResult = $ga->verifyCode($data->secret, $data->qrConfirmCode, 2);
if(!$checkResult){
    echo json_encode(['result' => 'fail']);
    return;
}


$hash_password = md5($data->password);
try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("INSERT INTO users(first_name, surname, mail, user_password, type, secret) VALUES(?,?, ?, ?, ?, ?)");
    $query->execute(array($data->name, $data->surname, $data->mail, $hash_password, $data->type, $data->secret));

}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}

echo json_encode(['result' => 'success']);
return;
