<?php
require_once("../../config.php");
require_once("../../vendor/autoload.php");
header('Content-type: application/json');

session_start();
session_unset();

$json = file_get_contents('php://input');

$client = new Google_Client();
try {
    $client->setAuthConfig('../../credentials.json');
} catch (\Google\Exception $e) {
    echo json_encode(['result' => 'fail']);
    return;
}
// Reset OAuth access token
$client->revokeToken();
//Destroy entire session
session_destroy();

echo json_encode(['result' => 'success']);