<?php
require_once("../../config.php");
require_once("../entities/UserEntity.php");
header('Content-type: application/json');

session_start();
$_SESSION["logged_id"] = null;

$json = file_get_contents('php://input');
$user = new UserEntity();

if($json === null) {
    echo json_encode(['result' => 'fail']);
    return;
}

$data = json_decode($json);

try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



    $query = $connection->prepare("SELECT users.id, users.user_password, users.secret FROM users WHERE users.mail=:mail AND users.type=:type");
    $query->execute(['mail' => $data->mail, 'type' => $data->type]);

    $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
    $allUsers = $query->fetchAll();
    if(sizeof($allUsers) < 1) {
        session_unset();
        throw new Exception();
    }

    $user = $allUsers[0];

    $hash_password = md5($data->password);
    if($hash_password !== $user->getPassword()){
        echo json_encode(['result' => 'fail_password']);
        return;
    }

}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}


require_once("../../PHPGangsta/GoogleAuthenticator.php");

$ga = new PHPGangsta_GoogleAuthenticator();
$checkResult = $ga->verifyCode($user->getSecret(), $data->qr, 2);
if(!$checkResult){
    echo json_encode(['result' => 'fail_qr']);
    return;
}


try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("INSERT INTO logins(id_user, timestamp) VALUES(:id_user, :timestamp)");
    $dateTime = new DateTime();
    $loginData = ['id_user' => $user->getId(), 'timestamp' => $dateTime->getTimestamp()];
    $query->execute($loginData);

}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}


$_SESSION["logged_id"] = $user->getId();

echo json_encode(['result' => 'success']);
return;
