<?php
header('Content-type: application/json');

session_start();
require_once("../../vendor/autoload.php");
require_once("../../config.php");
require_once("../entities/UserEntity.php");


$redirect_uri = 'https://wt118.fei.stuba.sk/logger-app/index.php';


$client = new Google_Client();
try {
    $client->setAuthConfig('../../credentials.json');
} catch (\Google\Exception $e) {
    echo json_encode(['result' => 'fail']);
    return;
}
$client->setRedirectUri($redirect_uri);
$client->addScope("email");
$client->addScope("profile");


$service = new Google_Service_Oauth2($client);


if(isset($_GET['code'])){
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
    $client->setAccessToken($token);
    $_SESSION['upload_token'] = $token;

    // redirect back to the example
    //header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

// set the access token as part of the client
if (!empty($_SESSION['upload_token'])) {
    $client->setAccessToken($_SESSION['upload_token']);
    if ($client->isAccessTokenExpired()) {
        unset($_SESSION['upload_token']);
    }
} else {
    $authUrl = $client->createAuthUrl();
}


if ($client->getAccessToken()) {
    //Get user profile data from google
    $UserProfile = $service->userinfo->get();
    if(!empty($UserProfile)){
        $google_id =  $UserProfile['id'];
        $name = $UserProfile['givenName'];
        $surname = $UserProfile['familyName'];
        $mail = $UserProfile['email'];



        try {
            $connection = new PDO("mysql:host=" . DB_HOST . "; dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $query = $connection->prepare("SELECT * FROM users WHERE users.google_id=:google_id");
            $query->execute(['google_id' => $google_id]);

            $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
            $allUsers = $query->fetchAll();


            if ($allUsers === null || sizeof($allUsers) < 1 ) {
                $query = $connection->prepare("INSERT INTO users(first_name, surname, mail, type, google_id) VALUES(?, ?, ?, ?, ?)");
                $query->execute(array($name, $surname, $mail, "google", $google_id));
            }


            $query = $connection->prepare("SELECT * FROm users WHERE users.type=:type AND users.mail=:mail");
            $query->execute(['type' => "google", 'mail' => $mail]);

            $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
            $user = $query->fetchAll()[0];

            $query = $connection->prepare("INSERT INTO logins(id_user, timestamp) VALUES(:id_user, :timestamp)");
            $dateTime = new DateTime();
            $loginData = ['id_user' => $user->getId(), 'timestamp' => $dateTime->getTimestamp()];
            $query->execute($loginData);

            $client->revokeToken();
            session_unset();

            $_SESSION["logged_id"] = $user->getId();


        }catch (Exception $exception){
            $client->revokeToken();
            session_unset();

            echo json_encode(['result' => 'fail', 'user' => $UserProfile]);
            return;
        }
        echo json_encode(['result' => 'success_login']);
    }else{
        $client->revokeToken();
        session_unset();
        echo json_encode(['result' => 'fail']);
    }
} else {
    $authUrl = $client->createAuthUrl();
    $loginLink = filter_var($authUrl, FILTER_SANITIZE_URL);

    echo json_encode(['result' => 'success_create_login', 'login_link' => $loginLink]);
}




