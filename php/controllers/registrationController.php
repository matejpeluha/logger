<?php
require_once("../../config.php");
require_once("../entities/UserEntity.php");
header('Content-type: application/json');

$json = file_get_contents('php://input');

if($json === null) {
    echo json_encode(['result' => 'fail']);
    return;
}

$data = json_decode($json);

try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("SELECT * FROm users WHERE users.type=:type AND users.mail=:mail");
    $query->execute(['type' => $data->type, 'mail' => $data->mail]);

    $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
    $allUsers = $query->fetchAll();

    if(sizeof($allUsers) >= 1)
        throw new Exception();
}catch (Exception $exception){
        echo json_encode(['result' => 'fail']);
        return;
}

require_once("../../PHPGangsta/GoogleAuthenticator.php");

$ga = new PHPGangsta_GoogleAuthenticator();
try {
    $secret = $ga->createSecret();
}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}
$qrCodeUrl = $ga->getQRCodeGoogleUrl('LOGGER-APP:' . $data->mail, $secret);

echo json_encode(['result' => 'success', 'secret' => $secret, 'qrCodeUrl' => $qrCodeUrl]);
return;

