<?php
require_once("../../config.php");
require_once("../entities/UserEntity.php");
header('Content-type: application/json');

session_start();
$_SESSION["logged_id"] = null;

$json = file_get_contents('php://input');
$user = new UserEntity();

if($json === null) {
    echo json_encode(['result' => 'fail']);
    return;
}

$data = json_decode($json);

$mailArray = explode("@", $data->mail);

if(sizeof($mailArray) != 2 || $mailArray[1] !== "stuba.sk"){
    echo json_encode(['result' => 'fail']);
    return;
}

$ldapuid = $mailArray[0];
$dn = "ou=People, DC=stuba, DC=sk";
$ldaprdn = "uid=$ldapuid, $dn";
$ldapconn = ldap_connect("ldap.stuba.sk");

if(!$ldapconn){
    echo json_encode(['result' => 'fail']);
    return;
}

$set = ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

try {
    $ldapBind = ldap_bind($ldapconn, $ldaprdn, $data->password);

    if (!$ldapBind)
        throw new Exception();
}
catch (Exception $exception){
    ldap_close($ldapconn);
    echo json_encode(['result' => 'fail']);
    return;
}

$results = ldap_search($ldapconn, $dn, "mail=*$data->mail*", array("givenname", "surname"),0,1);
$info = ldap_get_entries($ldapconn, $results);
$name = $info[0]["givenname"][0];
$surname = $info[0]["sn"][0];

$user = new UserEntity();
try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("SELECT * FROM users WHERE users.mail=:mail AND users.type=:type");
    $query->execute(['mail' => $data->mail, 'type' => $data->type]);

    $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
    $allUsers = $query->fetchAll();
    if(sizeof($allUsers) < 1) {
        $query = $connection->prepare("INSERT INTO users(first_name, surname, mail, type) VALUES(?, ?, ?, ?)");
        $query->execute(array($name, $surname, $data->mail, $data->type));

        $query = $connection->prepare("SELECT * FROM users WHERE users.mail=:mail AND users.type=:type");
        $query->execute(['mail' => $data->mail, 'type' => $data->type]);

        $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
        $allUsers = $query->fetchAll();
    }

    $user = $allUsers[0];


}catch (Exception $exception){
    ldap_close($ldapconn);
    echo json_encode(['result' => 'fail']);
    return;
}

ldap_close($ldapconn);

try {
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("INSERT INTO logins(id_user, timestamp) VALUES(:id_user, :timestamp)");
    $dateTime = new DateTime();
    $loginData = ['id_user' => $user->getId(), 'timestamp' => $dateTime->getTimestamp()];
    $query->execute($loginData);
}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}

$_SESSION["logged_id"] = $user->getId();

echo json_encode(['result' => 'success']);


