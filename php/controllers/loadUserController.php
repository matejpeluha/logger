<?php
require_once("../../config.php");
require_once("../entities/UserEntity.php");
require_once("../entities/LoginEntity.php");
require_once("../entities/LoginCounterEntity.php");
header('Content-type: application/json');

session_start();

$userId = null;
if(isset($_SESSION['logged_id']))
    $userId = $_SESSION['logged_id'];

if($userId === null) {
    session_unset();
    echo json_encode(['result' => 'fail_logged']);
    return;
}

$json = file_get_contents('php://input');

if($json === null) {
    echo json_encode(['result' => 'fail']);
    return;
}

$data = json_decode($json);


$user = new UserEntity();
$allLogins = [];

try{
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("SELECT users.first_name, users.surname, users.type, users.mail FROM users WHERE users.id=:id");
    $query->execute(['id' => $userId]);

    $query->setFetchMode(PDO::FETCH_CLASS, "UserEntity");
    $allUsers = $query->fetchAll();

    if(sizeof($allUsers) < 1) {
        session_unset();
        throw new Exception();
    }

    $user = $allUsers[0];
}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}

try{
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("SELECT logins.timestamp FROM logins WHERE logins.id_user=:id");
    $query->execute(['id' => $userId]);

    $query->setFetchMode(PDO::FETCH_CLASS, "LoginEntity");
    $allLoginsEntities = $query->fetchAll();

    foreach ( $allLoginsEntities as $login){
        array_push($allLogins, $login->getDate());
    }
}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}

//$login_counter = new LoginCounterEntity();
try{
    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = $connection->prepare("SELECT COUNT(case WHEN users.type = 'registered' THEN 1 else null end) AS registered_count, 
                                            COUNT(case WHEN users.type = 'ldap' THEN 1 else null end) AS ldap_count, 
                                            COUNT(case WHEN users.type = 'google' THEN 1 else null end) AS google_count 
                                            FROM users WHERE users.id 
                                            IN (SELECT logins.id_user FROM `logins` GROUP BY logins.id_user)");
    $query->execute();

    $query->setFetchMode(PDO::FETCH_CLASS, "LoginCounterEntity");
    $allLoginCounterEntities = $query->fetchAll();
    $login_counter = $allLoginCounterEntities[0];

}catch (Exception $exception){
    echo json_encode(['result' => 'fail']);
    return;
}

echo json_encode(['result' => 'success', 'name' => $user->getFirstName(),
                    'surname' => $user->getSurname(), 'mail' => $user->getMail(), 'type' => $user->getType(),
                    'logins' => $allLogins, 'counter' => $login_counter]);
return;
