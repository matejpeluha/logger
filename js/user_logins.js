window.addEventListener("load", function (){
    printContent();

    initButtons();
});

function initButtons(){
    const logoutButton = document.getElementById("logout-button");
    logoutButton.addEventListener("click", initLogoutButton);
}

function printContent(){
    const data = JSON.stringify({});

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/loadUserController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handlePrintContentXhr(xhr);
    }
}


function handlePrintContentXhr(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success') {
        document.getElementById("result-fail-header").style.display = "none";
        printData(jsonData);
    }
    else if(jsonData.result === 'fail'){
        document.getElementById("result-fail-header").style.display = "block";
    }
    else{
        window.open("index.php", "_self");
    }

}

function printData(json){
    printInfo(json);
    printLogins(json.logins)
    printLoginsCount(json.counter);
}

function printInfo(json){
    document.getElementById("header").innerText = json.name + " " + json.surname;
    document.getElementById("mail-header").innerText =  json.mail;
    document.getElementById("login-type").innerText = json.type;
}

function printLogins(logins){
    const table = document.getElementById("logins-table-body");
    for(let index = 0; index < logins.length; index++){
        table.innerHTML += "<tr><td>" + logins[index] + "</td></tr>";
    }
}

function printLoginsCount(counter){
    document.getElementById("registered-count").innerText = counter.registered_count;
    document.getElementById("ldap-count").innerText = counter.ldap_count;
    document.getElementById("google-count").innerText = counter.google_count;
}