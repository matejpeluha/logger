function initLogoutButton(){
    const data = JSON.stringify({});

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/logoutController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
         handleLogoutXhr(xhr);
    }
}

function handleLogoutXhr(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success')
        window.open("index.php", "_self");
}
