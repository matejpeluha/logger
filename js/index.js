window.addEventListener("load", function (){
    loadLoggedUser();
    initButtons();
    initInputs();
    initGoogleLogin();


})

function initGoogleLogin(){
    const data = JSON.stringify({});

    const urlParameters = window.location.search;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/googleLoginController.php" + urlParameters, !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleGoogleLoginXhr(xhr);
    }
    //const googleLoginButton = document.getElementById("a-google-login");

}

function handleGoogleLoginXhr(xhr){
    console.log(xhr.responseText);
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    // in case we reply back from server
    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === "success_create_login"){
        document.getElementById("a-google-login").setAttribute("href", jsonData["login_link"]);
    }
    else if(jsonData.result === "success_login"){
        window.open("user_page.html", "_self");
    }
    else{
        document.getElementById("result-fail-header").style.display = "block";
        document.getElementById("result-fail-header-info").style.display = "block";
        document.getElementById("result-fail-header-info").innerText = "ERROR: Neúspešné prihlásanie";
    }
}

function initInputs(){
    const aisMailInput = document.getElementById("ais-mail");
    aisMailInput.addEventListener("change", function (){
        handleMailValidation(aisMailInput);
    })

    const mailInput = document.getElementById("mail");
    mailInput.addEventListener("change", function (){
        handleMailValidation(mailInput);
    })
}

function initButtons(){
    const loggerButton = document.getElementById("logger-button");
    loggerButton.addEventListener("click", function (){
        setActiveForm(loggerButton);
    })

    const ldapButton = document.getElementById("ldap-button");
    ldapButton.addEventListener("click", function (){
        setActiveForm(ldapButton);
    })

    const googleButton = document.getElementById("google-button");
    googleButton.addEventListener("click", function (){
        setActiveForm(googleButton);
    })
}

function handleMailValidation(mailInput){
    if(!mailInput.value.includes("@"))
        mailInput.setCustomValidity("Nesprávny formát mailu");
    else
        mailInput.setCustomValidity("");
}

function setActiveForm(newActiveButton){
    document.getElementsByClassName("active-button")[0].classList.remove("active-button");
    const activeForm = document.getElementsByClassName("active-form")[0];
    activeForm.style.display = "none";
    activeForm.classList.remove("active-form");

    newActiveButton.classList.add("active-button");

    const buttonId = newActiveButton.getAttribute("id");
    const newActiveForm = getNewActiveForm(buttonId);
    if(newActiveForm === null)
        return;

    newActiveForm.style.display = "block";
    newActiveForm.classList.add("active-form");
}

function getNewActiveForm(buttonId){
    if(buttonId === "logger-button")
        return document.getElementById("login-form");
    else if (buttonId === "ldap-button")
        return document.getElementById("ldap-login-form");
    else if (buttonId === "google-button")
        return document.getElementById("google-login");

    return null;
}

function loadLoggedUser(){
    const data = JSON.stringify({});

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/loggedChecker.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (xhr.readyState !== 4 || xhr.status !== 200)
            return;

        const jsonData = JSON.parse(xhr.responseText);
        if(jsonData.result === 'success')
            window.open("user_page.html", "_self");
    }
}




function handleLoginSubmit(){
    const mail = document.getElementById("mail").value;
    const password = document.getElementById("password").value;
    const qr = document.getElementById("qr").value;

    const login = {'mail': mail, 'password': password, 'type': 'registered', 'qr': qr};
    const data = JSON.stringify(login);


    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/loginController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleLoginXhrStateChange(xhr);
    }
}


function handleLoginXhrStateChange(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    // in case we reply back from server
    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success')
        window.open("user_page.html", "_self");
    else if(jsonData.result === 'fail_password'){
        document.getElementById("result-fail-header").style.display = "block";
        document.getElementById("result-fail-header-info").style.display = "block";
        document.getElementById("result-fail-header-info").innerText = "Nesprávne heslo";
    }
    else if(jsonData.result === 'fail_qr'){
        document.getElementById("result-fail-header").style.display = "block";
        document.getElementById("result-fail-header-info").style.display = "block";
        document.getElementById("result-fail-header-info").innerText = "Nesprávny kód z Google Authenticator";
    }
    else{
        document.getElementById("result-fail-header").style.display = "block";
        document.getElementById("result-fail-header-info").style.display = "block";
        document.getElementById("result-fail-header-info").innerText = "Neexistujúci účet";
    }
}


function handleLdapLoginSubmit(){
    const mail = document.getElementById("ais-mail").value;
    const password = document.getElementById("ais-password").value;

    const login = {'mail': mail, 'password': password, 'type': 'ldap'};
    const data = JSON.stringify(login);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/ldapLoginController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleLoginXhrStateChange(xhr);
    }
}