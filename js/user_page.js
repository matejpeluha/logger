window.addEventListener("load", function (){
    loadData();
    initButtons();
});

function initButtons(){
    const logoutButton = document.getElementById("logout-button");
    logoutButton.addEventListener("click", initLogoutButton);
}

function loadData(){
    const data = JSON.stringify({});

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/loadUserController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleLoadDataXhr(xhr);
    }
}

function handleLoadDataXhr(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success') {
        document.getElementById("welcome").style.display = "block";
        document.getElementById("header").innerText = jsonData.name + " " + jsonData.surname;
        document.getElementById("mail-header").innerText = jsonData.type + ": " + jsonData.mail;
    }
    else if(jsonData.result === 'fail'){
        document.getElementById("result-fail-header").style.display = "block";
    }
    else{
        window.open("index.php", "_self");
    }

}