window.addEventListener("load", function (){
    initInputs();
})

function initInputs(){
    const passwordInput = document.getElementById("password");
    passwordInput.addEventListener("input", validatePassword);

    const mailInput = document.getElementById("mail");
    mailInput.addEventListener("input", validateMail);
}

function validatePassword(){
    const passwordInput = document.getElementById("password");
    const password = passwordInput.value;

    if(password.length < 5 || !hasLowerCase(password) || !hasUpperCase(password) || !hasNumber(password)) {
        passwordInput.setCustomValidity("Heslo musí obsahovať aspon 5 znakov, " +
            "z toho aspoň 1 číslo, aspoň 1 veľké písmeno a aspoň 1 malé písmeno.");
    } else {
      passwordInput.setCustomValidity("");
    }
}

function validateMail(){
    const mailInput = document.getElementById("mail");
    const mail = mailInput.value;

    if(!mail.includes("@")) {
        mailInput.setCustomValidity("Zlý formát mailu");
    } else {
        mailInput.setCustomValidity("");
    }
}

function hasLowerCase(word) {
    return word.toUpperCase() !== word;
}

function hasUpperCase(word) {
    return word.toLowerCase() !== word;
}

function hasNumber(word) {
    return /\d/.test(word);
}


function handleRegistrationSubmit(){
    const mail = document.getElementById("mail").value;
    const registration = { 'mail': mail, 'type': "registered"};
    const data = JSON.stringify(registration);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/registrationController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleRegistrationXhr(xhr);
    }
}

function handleRegistrationXhr(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success') {
        document.getElementById("register-form").style.display = "none";
        document.getElementById("two-fa-registration").style.display = "block";
        document.getElementById("manual-code").innerText = "Kľúč: " + jsonData.secret;
        document.getElementById("qr-code").setAttribute("src", jsonData.qrCodeUrl);
        document.getElementById("result-fail-header").style.display = "none";
        document.getElementById("two-fa-button").addEventListener("click", function (){
            handleTwoFARegistration(jsonData);
        })
    }
    else
        document.getElementById("result-fail-header").style.display = "block";

}

function handleTwoFARegistration(json){
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const mail = document.getElementById("mail").value;
    const password = document.getElementById("password").value;
    const qrConfirmCode = document.getElementById("two-fa-code").value;

    const registration = {'name': name, 'surname': surname, 'mail': mail, 'password': password, 'type': "registered",
                            'secret': json.secret, 'qrConfirmCode': qrConfirmCode};
    const data = JSON.stringify(registration);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "php/controllers/registrationTwoFAController.php", !0);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        handleTwoFARegistrationXhr(xhr);
    }
}

function handleTwoFARegistrationXhr(xhr){
    if (xhr.readyState !== 4 || xhr.status !== 200)
        return;

    const jsonData = JSON.parse(xhr.responseText);
    if(jsonData.result === 'success')
        window.open("index.php", "_self");
    else
        document.getElementById("qr-result-fail-header").style.display = "block";
}

