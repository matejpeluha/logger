<?php
if(isset($_SESSION["logged_id"]))
    header('Location: user_page.html');
?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1"/>
    <title>Logger</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <script src="js/index.js"></script>

</head>
<body>

<h1 id="header">Prihlásenie</h1>
<h3 id="result-fail-header">Neúspešné prihlásenie</h3>
<h3 id="result-fail-header-info">Neúspešné prihlásenie</h3>
<main>

    <div id="login-div">

        <div id="login-chooser">
            <button id="logger-button" class="active-button">
                Registrovaný
            </button>
            <button id="ldap-button">
                LDAP AIS
            </button>
            <button id="google-button">
                Google
            </button>
        </div>


        <form id="login-form" class="active-form" action="javascript:handleLoginSubmit()" method="post">
            <div id="registration">
                <a id="registration-link" href="registration.html"><i class="fas fa-pencil-alt"></i> Registrovať</a>
            </div>
            <label>
                <input type="text" placeholder="E-mail" id="mail" name="mail"  required>
            </label><br>
            <label>
                <input type="password" placeholder="Heslo" id="password" name="password"  required >
            </label><br>
            <label>
                <input type="text" placeholder="Overovací kód Google Authenticator" id="qr" name="qr"  required>
            </label><br>

            <button type="submit">
                Prihlásiť
            </button>
        </form>

        <form id="ldap-login-form" action="javascript:handleLdapLoginSubmit()" method="post">
            <label>
                <input type="text" placeholder="Ais e-mail @stuba.sk" id="ais-mail" name="ais-mail" required>
            </label><br>
            <label>
                <input type="password" placeholder="Ais Heslo" id="ais-password" name="ais-password"  required>
            </label><br>


            <button type="submit">
                Prihlásiť
            </button>
        </form>

        <div id="google-login">
            <a id="a-google-login" href=""><img src="images/glogin.png" alt="google login"></a>
        </div>
    </div>

</main>

</body>
</html>